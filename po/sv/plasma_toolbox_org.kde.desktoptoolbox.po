# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2014, 2015, 2019, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-17 00:38+0000\n"
"PO-Revision-Date: 2021-10-21 16:12+0200\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.1\n"

#: contents/ui/ToolBoxContent.qml:268
#, kde-format
msgid "Choose Global Theme…"
msgstr "Välj globalt tema…"

#: contents/ui/ToolBoxContent.qml:275
#, kde-format
msgid "Configure Display Settings…"
msgstr "Anpassa skärminställningar…"

#: contents/ui/ToolBoxContent.qml:296
#, kde-format
msgctxt "@action:button"
msgid "More"
msgstr ""

#: contents/ui/ToolBoxContent.qml:310
#, kde-format
msgid "Exit Edit Mode"
msgstr "Avsluta redigeringsläge"

#~ msgid "Finish Customizing Layout"
#~ msgstr "Slutför anpassning av layout"

#~ msgid "Default"
#~ msgstr "Förval"

#~ msgid "Desktop Toolbox"
#~ msgstr "Skrivbordsverktyglåda"

#~ msgid "Desktop Toolbox — %1 Activity"
#~ msgstr "Skrivbordsverktyglåda — Aktivitet %1"

#~ msgid "Lock Screen"
#~ msgstr "Lås skärmen"

#~ msgid "Leave"
#~ msgstr "Gå ifrån"
