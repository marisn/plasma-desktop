# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Volkan Gezer <volkangezer@gmail.com>, 2021.
# Emir SARI <emir_sari@icloud.com>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-19 01:34+0000\n"
"PO-Revision-Date: 2023-11-14 18:00+0300\n"
"Last-Translator: Emir SARI <emir_sari@icloud.com>\n"
"Language-Team: Turkish <kde-l10n-tr@kde.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 23.11.70\n"

#: contents/ui/ConfigOverlay.qml:277 contents/ui/ConfigOverlay.qml:311
#, kde-format
msgid "Remove"
msgstr "Kaldır"

#: contents/ui/ConfigOverlay.qml:287
#, kde-format
msgid "Configure…"
msgstr "Yapılandır…"

#: contents/ui/ConfigOverlay.qml:298
#, kde-format
msgid "Show Alternatives…"
msgstr "Alternatifleri Göster…"

#: contents/ui/ConfigOverlay.qml:321
#, kde-format
msgid "Spacer width"
msgstr "Boşluklandırıcı genişliği"

#: contents/ui/main.qml:388
#, kde-format
msgid "Add Widgets…"
msgstr "Araç Takımları Ekle…"
