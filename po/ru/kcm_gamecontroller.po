# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# SPDX-FileCopyrightText: 2023 Alexander Yavorsky <kekcuha@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-29 01:38+0000\n"
"PO-Revision-Date: 2023-12-17 10:04+0300\n"
"Last-Translator: Alexander Yavorsky <kekcuha@gmail.com>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 23.08.4\n"

#: axesmodel.cpp:63
#, kde-format
msgctxt "@label Axis value"
msgid "Value"
msgstr "Значение"

#: buttonmodel.cpp:74
#, kde-format
msgctxt "Status of a gamepad button"
msgid "PRESSED"
msgstr "НАЖАТА"

#: buttonmodel.cpp:84
#, kde-format
msgctxt "@label Button state"
msgid "State"
msgstr "Состояние"

#: devicemodel.cpp:51
#, kde-format
msgctxt "Device name and path"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: ui/main.qml:22
#, kde-format
msgid "No game controllers found"
msgstr "Не найдено ни одного игрового контроллера"

#: ui/main.qml:54
#, kde-format
msgctxt "@label:textbox"
msgid "Device:"
msgstr "Устройство:"

#: ui/main.qml:80
#, kde-format
msgctxt "@label Visual representation of an axis position"
msgid "Position:"
msgstr "Положение:"

#: ui/main.qml:98
#, kde-format
msgctxt "@label Gamepad buttons"
msgid "Buttons:"
msgstr "Кнопки:"

#: ui/main.qml:119
#, kde-format
msgctxt "@label Gamepad axes (sticks)"
msgid "Axes:"
msgstr "Оси:"
